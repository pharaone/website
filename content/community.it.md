+++
title = "Comunitá"
+++

Questa pagina prova a spiegare come é organizzata la comunitá di Redox OS e ti aiuterá a navigarvici. Le intestazioni sono cliccabili per aiutarti a trovare ciò che stai cercando.

Seguiamo il [Codice di condotta di Rust](https://www.rust-lang.org/policies/code-of-conduct) per le regole in tutti i canali di comunità/chat.

## [Comunicazioni](https://matrix.to/#/#redox-announcements:matrix.org)

Pubblichiamo le nostre comunicazioni in [questa](https://matrix.to/#/#redox-announcements:matrix.org) stanza di Matrix, è pubblica e non è necessario accedere per leggerla.

- #redox-announcements:matrix.org (Usa questo indirizzo della stanza se non vuoi usare il link Matrix esterno)

## [Chat](https://matrix.to/#/#redox-join:matrix.org)

Matrix è il metodo ufficiale per comunicare con il team di Redox OS e la comunità (queste stanze sono solo in Inglese, non sono accettati altre lingue perché non li conosciamo)

Matrix ha diversi client. [Element](https://element.io/) è molto utilizzato, funziona sui browser, Linux, MacOSX, Windows, Android ed iOS.

Se riscontri problemi con Element, prova [Fractal](https://gitlab.gnome.org/World/fractal).

- Entra in [questa](https://matrix.to/#/#redox-join:matrix.org) stanza e non dimenticare di richiedere un invito allo spazio Matrix Redox.
- #redox-join:matrix.org (Usa questo indirizzo della stanza se non vuoi usare il link Matrix esterno)

(Suggeriamo di uscire dalla stanza "Join Requests" dopo essere entrato nello spazio Redox)

Se vuoi fare una grande discussione nelle nostre stanze, usa un thread di Element, è più organizzato ed è più facile tenere traccia delle varie discussioni che avvengono nella stessa stanza.

Puoi trovare più informazioni in [questa](https://doc.redox-os.org/book/chat.html) pagina.

## [Discord](https://discord.gg/JfggvrHGDY)

Abbiamo un server Discord in alternativa a quello Matrix.

(I messaggi Matrix vengono inviati su Discord e quelli Discord vengono inviati su Matrix grazie ad un bot)

## [Summer of Code](/rsoc)

Il programma **Redox Summer of Code** (RSoC) si svolge annualmente quando i fondi lo permettono, e potremmo partecipare anche ad altri programmi simili al Summer of Code.  
Una panoramica dei nostri programmi Summer of Code e dei nostri piani per quest'anno sono disponibili [qui](/rsoc).  
Dai un'occhiata alla nostra [Guida per le Proposte RSoC](/rsoc-proposal-how-to) e ai [Suggerimenti per i Progetti](/rsoc-project-suggestions).

## [GitLab](https://gitlab.redox-os.org/redox-os/redox)

Un modo di comunicare leggermente più formale con gli altri sviluppatori di Redox, ma un po' meno rapido e conveniente rispetto alla chat. Invia un issue (una segnalazione di un problema) quando riscontri problemi durante la compilazione o il testing. Gli issue possono essere utilizzati anche se desideri discutere di un determinato argomento: che si tratti di funzionalità, stile del codice, incongruenze del codice, modifiche e correzioni minori, ecc.

Se vuoi creare un account, leggi questa [pagina](https://doc.redox-os.org/book/signing-in-to-gitlab.html).

Una volta creato un issue, non dimenticare di postare il link nelle stanze Dev o Support della chat, perché le notifiche email di GitLab contengono spesso distrazioni (messaggi di servizio o spam) e la maggior parte degli sviluppatori non lascia aperte le pagine di GitLab per ricevere notifiche desktop dal browser (che richiedono una configurazione personalizzata per ricevere notifiche sugli issue).

Facendo ciò, ci aiuti a prestare attenzione ai tuoi issue e ad evitare che vengano dimenticati accidentalmente.

Se hai pronte delle MR (*merge requests*), devi inviare i link nella stanza [MRs](https://matrix.to/#/#redox-mrs:matrix.org). Per unirti a questa stanza, dovrai richiedere un invito nella stanza [Join Requests](https://matrix.to/#/#redox-join:matrix.org).

Inviando un messaggio nella stanza, la tua MR non verrà dimenticata e non accumulerà conflitti.

## [Lemmy](https://lemmy.world/c/redox)

La nostra alternativa a Reddit, dove postiamo notizie e discussioni della comunità.

## [Redox OS su Reddit](https://www.reddit.com/r/Redox/)

Se vuoi semplicemente dare un occhiata veloce a quello che succede e parlarne.

[reddit.com/r/rust](https://www.reddit.com/r/rust) per le correlate news e discussioni su Rust.

## [Fosstodon](https://fosstodon.org/@redox)

La nostra alternativa a Twitter, dove pubblichiamo notizie e discussioni della comunità.

## [Twitter](https://twitter.com/redox_os)

Notizie e discussioni della comunità.

## [YouTube](https://www.youtube.com/@RedoxOS)

Demo e riunioni del consiglio.

## [Forum](https://discourse.redox-os.org/)

Questo è il nostro forum archiviato con vecchie domande; è inattivo e può essere utilizzato solo come archivio. Se hai una domanda, inviala sulla chat di Matrix.

## [Talks](/talks/)

Talk di Redox tenute in vari eventi e conferenze.

## Diffondi la notizia

La divulgazione nella comunità è una parte importante del successo di Redox. Se più persone conoscono Redox, è più probabile che nuovi contributori si uniscano e tutti possano beneficiare della loro esperienza aggiuntiva. Puoi fare la differenza scrivendo articoli, parlando con altri appassionati di sistemi operativi o cercando comunità interessate a saperne di più su Redox.
