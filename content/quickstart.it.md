+++
title = "Guida rapida"
+++

- [Ultimo rilascio](https://www.redox-os.org/news/release-0.9.0/)
- [Rilasci](https://gitlab.redox-os.org/redox-os/redox/-/releases)
- [Immagini giornaliere](https://static.redox-os.org/img/)
- [Come iniziare](https://doc.redox-os.org/book/getting-started.html)

## Provare Redox

Provare Redox è molto facile segui le istruzioni nel[Libro di Redox](https://doc.redox-os.org/book/trying-out-redox.html). Far girare Redox su macchine fisiche è possibile, ma le performance potrebbero variare. Controlla la pagina [macchine supportate](https://gitlab.redox-os.org/redox-os/redox/-/blob/master/HARDWARE.md) per avere più informazioni.

## Compilare Redox

La compilazione di Redox dal codice sorgente è supportata su Linux e potrebbe funzionare su alcune altre piattaforme, ma attualmente non è supportata su Windows. Per compilare Redox, leggi [questa](https://doc.redox-os.org/book/podman-build.html) pagina.
