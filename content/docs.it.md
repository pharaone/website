+++
title = "Documentazione"
+++

## Iniziamo

Noi ti consigliamo di iniziare con [Book](https://doc.redox-os.org/book/), che descrive come installare Redox e come usarlo.

## Riferimenti

### Cargo Docs

- [libredox](https://docs.rs/libredox/latest/libredox/) - Documentazione della liberia di sistema di Redox.

- [libstd](https://doc.rust-lang.org/stable/std/) - Documentazione della libreria standard di Rust.

### Gitlab

- [Gitlab](https://gitlab.redox-os.org/) - Repository GitLab di Redox OS dove puoi trovare tutto il codice sorgente.

- [Drivers](https://gitlab.redox-os.org/redox-os/drivers/-/blob/master/README.md) - Documentazione di alto livello dei driver.

- [RFCs](https://gitlab.redox-os.org/redox-os/rfcs) - Richiesta di cambiamenti in Redox.

- [Ion Manual](https://doc.redox-os.org/ion-manual/) - Documentazione della shell Ion.

### Talks

- [Talks](/talks/) - Talk su Redox effettuate in podcast e conferenze.

## Contribuire a Redox

- Leggi il [CONTRIBUTING.md](https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md)
