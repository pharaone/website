+++
title = "Redox in azione"
+++

## I programmi COSMIC in esecuzione su Redox OS
<img class="img-responsive" src="/img/screenshot/cosmic-programs.png"/>

## Screenfetch in esecuzione su Redox OS
<img class="img-responsive" src="/img/screenshot/cosmic-term-screenfetch.png"/>

## Visualizzatore di immagini su Redox OS
<img class="img-responsive" src="/img/screenshot/image-viewer.png"/>

## NetSurf in esecuzione su Redox OS
<img class="img-responsive" src="/img/screenshot/netsurf.png"/>

## Gears di SDL2 in esecuzione su Redox OS
<img class="img-responsive" src="/img/screenshot/sdl2-gears.png"/>

## La intro di Stargate SG-1 Stagione 10 in riproduzione su Redox OS
<iframe width="560" height="315" src="https://www.youtube.com/embed/3cPekY4c9Hc?si=EYuAkgpVDAKOb0jW" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Mesa3D Gears con LLVMpipe in esecuzione su Redox OS
<iframe width="560" height="315" src="https://www.youtube.com/embed/ADSvEA_YY7E?si=vEtlU2rGxJldIFuk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Pixelcannon in esecuzione su Redox OS
<img class="img-responsive" src="/img/screenshot/pixelcannon.png"/>

## System76 Galago Pro (galp3-c)
<img class="img-responsive" src="/img/hardware/system76-galp3-c.jpg"/>

## Panasonic Toughbook CF-18
<img class="img-responsive" src="/img/hardware/panasonic-toughbook-cf18.png"/>

## ASUS eeePC 900
<img class="img-responsive" src="/img/hardware/asus-eepc-900.png"/>

## ThinkPad T420
<img class="img-responsive" src="/img/hardware/thinkpad-t420.png"/>

## ThinkPad T520, ThinkPad P50 e Asus P7P55D-E Pro Desktop
<img class="img-responsive" src="/img/hardware/T520-P50-Asus-Desktop.jpg"/>
