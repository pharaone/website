+++
title = "Donare/merchandising"
+++

## Redox OS Nonprofit

Redox OS ha un'organizzazione non profit 501(c)(4) in Colorado (USA) che gestisce le donazioni. Le donazioni alla non profit Redox OS saranno utilizzate come deciso dal consiglio di amministrazione dei volontari di Redox OS.

Puoi donare a Redox OS nei seguenti modi:

- [Patreon](https://www.patreon.com/redox_os)  
- [Donorbox](https://donorbox.org/redox-os)  
- [Bitcoin](https://bitcoin.org/) - 3NhKNtLMBg7xvU3AeEQBxKii1Qm72R6pWg  
- [Ethereum](https://ethereum.org/en/) - 0x083e29156955A4c0f7eAA406e1167Bd1bE88933E  
- Per ulteriori opzioni di donazione, contatta donate@redox-os.org  

## Merchandising

Vendiamo magliette su Teespring, puoi acquistarle [qui](https://redox-os.creator-spring.com/).

Ogni vendita è una donazione alla non profit Redox OS.

## Jeremy Soller

Jeremy Soller è il creatore, manutentore e sviluppatore principale di Redox OS. Le donazioni a lui sono trattate come un dono tassabile e saranno utilizzate a sua discrezione.

Puoi donare a Jeremy nei seguenti modi:

- [Liberapay](https://liberapay.com/redox_os)  
- [Paypal](https://www.paypal.me/redoxos)  
- Jeremy non accetta più donazioni in Bitcoin o Ethereum. Non donare agli indirizzi dei wallet elencati sul sito in passato, poiché la donazione non verrà ricevuta.

I seguenti sostenitori hanno donato 4$ o più a Jeremy per lo sviluppo di Redox OS:  
{{% partial "donors/jackpot51.html" %}}

